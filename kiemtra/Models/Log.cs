﻿namespace kiemtra.Models
{
    public class Log
    {
        public int Id { get; set; }

        public DateTime? Logindate { get; set; }

        public DateTime? Logouttime { get; set; }

        public ICollection<Report>? Reports { get; set; }    

    }
}
