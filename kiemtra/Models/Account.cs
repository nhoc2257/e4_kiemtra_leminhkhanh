﻿namespace kiemtra.Models
{
    public class Account
    {
        public int Id { get; set; }

        public ICollection<Report> Reports { get; set; }

        public string Accountname { get; set; }


    }
}
