﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace kiemtra.Models
{
    public class Report
    {
        public int Id { get; set; }
        public string Reportname { get; set; }
        public DateTime Reportdate { get; set; }
    }
}
