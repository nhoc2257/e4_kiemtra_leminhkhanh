﻿namespace kiemtra.Models
{
    public class Transaction
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public ICollection<Report> Reports { get; set; }

        public ICollection<Log> Logs { get; set; }
    }
}
